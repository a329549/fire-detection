from ctypes import resize
import numpy as np
from flask import Flask, request, jsonify, render_template, url_for
import streamlit as st
from keras.applications import xception
import cv2
from keras.models import load_model
from PIL import Image
from skimage.transform import resize

# Path del modelo preentrenado
MODEL_PATH = './modelo_chido.h5'

def create_mask_for_plant(image):
    image_hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    lower_hsv = np.array([0,0,250])
    upper_hsv = np.array([250,255,255])
    
    #utiliza una máscara para convertir la imagen a formato de color hsv
    mask = cv2.inRange(image_hsv, lower_hsv, upper_hsv)
    #estructura los arreglos (imagenes) para que funcionen en elipses
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11,11))
    #rellena partes de la imágen para que sea más clara
    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
    
    return mask

# Para hacer segmentación
def segment_image(image):
    mask = create_mask_for_plant(image)
    #combina dos imagenes
    output = cv2.bitwise_and(image, image, mask = mask)
    return output/255

# Para hacer un "sharpen"
def sharpen_image(image):
    
    #se hace más suave la imágen
    image_blurred = cv2.GaussianBlur(image, (0, 0), 3)
    
    #se hace una combinación entre la imagen suave y la inicial
    image_sharp = cv2.addWeighted(image, 1.5, image_blurred, -0.5, 0)
    
    return image_sharp

# Se recibe la imagen y el modelo, devuelve la predicción
def model_prediction(image_path, model):

    #image_path = 'A:/descargas/archive/fire_dataset/non_fire_images/non_fire.99.png'

    INPUT_SIZE=255

    #preprocesa la entrada
    xt = np.zeros((1, INPUT_SIZE, INPUT_SIZE, 3), dtype='float')
    #lee la imagen
    ima = cv2.resize(image_path, (255,255))
    #ima = resize(image_path, (255,255))
    
    
    #mascara y segmentacion
    image_seg = segment_image(ima)
    #sharpen
    image_sharp = sharpen_image(image_seg)
    #se expande la imagen "sharpen" dentro de cada fila de un arreglo
    xt = xception.preprocess_input(np.expand_dims(image_sharp.copy(), axis=0))
    xception_bf = xception.Xception(weights='imagenet', include_top=False, pooling='avg')

    im = xception_bf.predict(xt, batch_size=32, verbose=1)
    pred = (model.predict(im) > 0.5).astype("int32")

    return pred[0][0]

def main():
    
    model = load_model(MODEL_PATH)

    # Título
    #html_temp = """
    #<h1 style="color:#181082;text-align:center;">SISTEMA DE BÚSQUEDA DE FUEGO</h1>
    #</div>
    #"""
    st.title("SISTEMA DE BÚSQUEDA DE FUEGO")
    #st.markdown(html_temp,unsafe_allow_html=True)

    # Lecctura de datos
    #Datos = st.text_input("Ingrese los valores : N P K Temp Hum pH lluvia:")
    
    uploaded_file = st.file_uploader("Choose an image", ["jpg","jpeg","png"]) #image uploader
    if uploaded_file is not None:
        file_bytes = np.asarray(bytearray(uploaded_file.read()), dtype=np.uint8)
        image = cv2.imdecode(file_bytes, 1)
        #image = np.array(Image.open(uploaded_file), dtype=np.uint8)   
        image = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
        st.image(image, caption="Imagen", use_column_width=False)

    

    # El botón predicción se usa para iniciar el procesamiento
    if st.button("Predicción"): 
        #x_in = list(np.float_((Datos.title().split('\t'))))
        predictS = model_prediction(image, model)
        if predictS == 0:
            st.success('Fuego')
        else:
            st.success('No hay fuego')

if __name__ == '__main__':
    main()