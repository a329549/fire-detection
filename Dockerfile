FROM python:3.7.13

# copy into a directory of its own (so it isn't in the toplevel dir)
COPY . /app
WORKDIR /app

# remember to expose the port your app'll be exposed on.
EXPOSE 8080

RUN pip install -U pip
RUN sudo apt-get install ffmpeg libsm6 libxext6  -y

COPY requirements.txt app/requirements.txt
RUN pip install -r app/requirements.txt

# run it!
ENTRYPOINT ["streamlit", "run", "main.py", "--server.port=8080", "--server.address=0.0.0.0"]