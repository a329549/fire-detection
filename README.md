# 🔥 Fire detection

## Instalación
```
git clone https://gitlab.com/a329549/fire-detection.git
cd ./fire_detection
pip install -r app/requirements.txt
streamlit run main.py
```

## 📃 License
MIT
